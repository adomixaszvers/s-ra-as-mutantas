Unit Sarasas;

interface

Uses Bazinis;

type

Busena = (Gerai, Nera, PerzengtiReziai, NeraVietos, Perstumiama);

SarasoTipas = record
	dydis: Integer;
	elementu: Integer;
	masyvas: array of BazinisTipas;
end;

procedure Initialize(var L: SarasoTipas; dydis: Integer);
function Insert(var L: SarasoTipas; i: Integer): Busena;
function Insert(var L: SarasoTipas; x: BazinisTipas; i: Integer): Busena;
function Delete(var L: SarasoTipas; i: Integer): Busena;
function Retrieve(L: SarasoTipas; i: Integer): Busena;
procedure Kill(var L: SarasoTipas);
procedure Print(L: SarasoTipas);
procedure TikrintiBusena(b: Busena);
procedure SkaitymasIsFailo(var L: SarasoTipas; var df: Text);

implementation

procedure Initialize(var L: SarasoTipas; dydis: Integer);
begin
	L.dydis := dydis;
	L.elementu := 0;
	SetLength(L.masyvas, dydis);
end;

function Retrieve(L: SarasoTipas; i: Integer): Busena;
begin
	if (i >= L.elementu) or (i < 0) then
	begin
		{ WriteLn('Tokio elemento nera'); }
		Retrieve := Nera;
	end
	else
	begin
		Isvesti(L.masyvas[i]);
		Retrieve := Gerai;
	end;
end;

procedure Kill(var L: SarasoTipas);
var
	i: Integer;
begin
	L.dydis := 0;
	L.elementu := 0;
	SetLength(L.masyvas, 0);
end;

function Insert(var L: SarasoTipas; x: BazinisTipas; i: Integer): Busena;
var
	k: Integer;
begin
	{ WriteLn('Iterpimas'); }
	if (i >= L.dydis) or (i < 0) then
	begin
		{ WriteLn('Perzengti reziai'); }
		Insert := PerzengtiReziai;
		Exit;
	end
	else if L.elementu = L.dydis then
	begin
		{ WriteLn('Neuztenka vietos'); }
		Insert := NeraVietos;
		Exit;
	end
	else if i >= L.elementu then
	begin
		{ WriteLn('Iterpiame gale'); }
		L.masyvas[L.elementu] := x;
		Insert := Gerai;
	end
	else
	begin
		{ WriteLn('Perstumiami elementai'); }
		for k := L.elementu-1 downto i do
			L.masyvas[k+1] := L.masyvas[k];
		L.masyvas[i] := x;
		Insert := Perstumiama;
	end;
	L.elementu := L.elementu + 1;
end;

function Insert(var L: SarasoTipas; i: Integer): Busena;
var
	x: BazinisTipas;
begin
	Ivesti(x);
	Insert := Insert(L, x, i);
end;

function Delete(var L: SarasoTipas; i: Integer): Busena;
var
	k: Integer;
begin
	{ WriteLn('Trinimas'); }
	if (i >= L.dydis) or (i < 0) then
	begin
		{ WriteLn('Perzengti reziai'); }
		Delete := PerzengtiReziai;
		Exit;
	end
	else if L.elementu = 0 then
	begin
		{ WriteLn('Nera ko trinti'); }
		Delete := Nera;
		Exit;
	end
	else if i >= L.elementu-1 then
	begin
		{ WriteLn('Trinamas paskutinis elementas'); }
		L.elementu := L.elementu - 1;
		Delete := Gerai;
	end
	else
	begin
		{ WriteLn('Perstumiami elementai'); }
		for k := i + 1 to L.elementu - 1 do
			L.masyvas[k-1] := L.masyvas[k];
		L.elementu := L.elementu-1;
		Delete := Perstumiama;
	end;
end;

procedure Print(L: SarasoTipas);
var i: Integer;
begin
	for i := 0 to L.elementu - 1 do
	begin
		 WriteLn('Elementas Nr. ', i);
		Isvesti(L.masyvas[i]);
		WriteLn;
	end;
	
	WriteLn('*********');
	WriteLn('Atbulai');
	WriteLn('*********');

	for i := L.elementu - 1 downto 0 do
	begin
		 WriteLn('Elementas Nr. ', i);
		Isvesti(L.masyvas[i]);
		WriteLn;
	end;
end;

procedure TikrintiBusena(b: Busena);
begin
	case b of
	Gerai: WriteLn(''); { galima vesti ka nors, jog ale viskas gerai}
	Nera: WriteLn('Nerasta');
	NeraVietos: WriteLn('Neliko vietos');
	PerzengtiReziai: WriteLn('Perzengti reziai');
	Perstumiama: WriteLn('Perstumiama');
	end;
end;

procedure SkaitymasIsFailo(var L: SarasoTipas; var df: Text);
var
	n, dydis, i: Integer;
	x: BazinisTipas;
	ch: char;
begin
	Reset(df);

	ReadLn(df, n, dydis);
	if (n < 0) or (dydis < 0) then
		WriteLn('Reiksmes negali buti neigiamos')
	else if (n > dydis) then
		WriteLn('Per daug elementu')
	else
	begin
		Initialize(L, dydis);
		for i := 0 to n-1 do
		begin
			ReadLn(df, x.pavadimas);
			ReadLn(df, x.gyv_skaicius);
			ReadLn(df, x.plotas);
			ReadLn(df, x.zemynas);
			ReadLn(df, ch);
			if ch = 'y' then
				x.ar_es := true
			else x.ar_es := false;
			ReadLn(df);
			Insert(L, x, i); 
		end;
	end;
	Close(df);
end;

begin
end.
