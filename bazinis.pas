Unit Bazinis;

interface


type

Salis = record
	pavadimas: String;
	gyv_skaicius: Longint;
	plotas: Real;
	zemynas: String;
	ar_es: Boolean;
end;

BazinisTipas = Salis;

procedure Ivesti(var x: Salis);
procedure Isvesti(x: Salis);

implementation

procedure Ivesti(var x: Salis);
var
	ch: char;
begin
	WriteLn;
	WriteLn('Ivedami salies duomenys');
	Write('Pavadinimas: ');
	ReadLn(x.pavadimas);
	Write('Gyventoju skaicius: ');
	ReadLn(x.gyv_skaicius);
	Write('Plotas: ');
	ReadLn(x.plotas);
	Write('Zemynas: ');
	ReadLn(x.zemynas);
	Write('Ar Europos Sajungoje? (y/n) ');
	ReadLn(ch);
	if ch = 'y' then
		x.ar_es := true
	else
		x.ar_es := false;
	WriteLn('=====');
end;

procedure Isvesti(x: Salis);
begin
	WriteLn('Isvedami salies duomenys');
	Write('Pavadinimas: ');
	WriteLn(x.pavadimas);
	Write('Gyventoju skaicius: ');
	WriteLn(x.gyv_skaicius);
	Write('Plotas: ');
	WriteLn(x.plotas:0:2);
	Write('Zemynas: ');
	WriteLn(x.zemynas);
	Write('Ar Europos Sajungoje? (y/n) ');
	if x.ar_es then
		WriteLn('y')
	else
		WriteLn('n');
	WriteLn('=====');
end;

begin
end.
