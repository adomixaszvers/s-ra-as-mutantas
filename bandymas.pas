program Bandymas;

Uses Sarasas;

var
	sar: SarasoTipas;
begin
	Initialize(sar, 10);
	Insert(sar, 0);
	Insert(sar, 0);
	Insert(sar, 5);
	Print(sar);
	Retrieve(sar, 1);
	Delete(sar, 1);
	Print(sar);
	Kill(sar);
end.
