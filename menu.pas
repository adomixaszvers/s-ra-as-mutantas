program menu;

Uses Sarasas;

const dfPav = 'duomenys.txt';

var
	sar: SarasoTipas;
	nr, nr2: Integer;
	df: Text;
begin
	sar.dydis := 0;
	Assign(df, dfPav);
	while True do
	begin
		if sar.dydis = 0 then
		begin
			WriteLn('1. Initialize(L)');
			WriteLn('2. Nuskaityti is failo');
			WriteLn('0. Iseiti');
			Write('Jusu pasirinkimas: ');
			ReadLn(nr);
			case nr of
			1: begin
				Write('Saraso dydis: ');
				ReadLn(nr2);
				if nr2 < 1 then
				begin
					WriteLn('Netinkamas saraso dydis');
					continue;
				end;
				Initialize(sar, nr2);
			end;
			2: SkaitymasIsFailo(sar, df);
			0: break;
			else WriteLn('Netinkama ivestis');
			end;
		end
		else
		begin
			WriteLn('1. Insert(L, x, i)');
			WriteLn('2. Delete(L, i)');
			WriteLn('3. Retrieve(L, i)');
			WriteLn('4. Print(L)');
			WriteLn('5. Kill(L)');
			WriteLn('0. Iseiti');
			Write('Jusu pasirinkimas: ');
			ReadLn(nr);
			case nr of
			1: begin
				Write('i: ');
				ReadLn(nr2);
				TikrintiBusena(Insert(sar, nr2));
			end;
			2: begin
				Write('i: ');
				ReadLn(nr2);
				TikrintiBusena(Delete(sar, nr2));
			end;
			3: begin
				Write('i: ');
				ReadLn(nr2);
				TikrintiBusena(Retrieve(sar, nr2));
			end;
			4: Print(sar);
			5: Kill(sar);
			0: break;
			else WriteLn('Netinkama ivestis');
			end;
		end;
	end;
	if sar.dydis > 0 then
		Kill(sar);
end.
